module github.com/masudur-rahman/expense-tracker-bot

go 1.21

require (
	github.com/graphql-go/graphql v0.8.1
	github.com/jedib0t/go-pretty/v6 v6.4.6
	github.com/masudur-rahman/database v0.0.0-20230712191307-c3b5f6c5cd9b
	github.com/masudur-rahman/go-oneliners v1.0.0
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/rs/xid v1.5.0
	github.com/spf13/cobra v1.7.0
	github.com/spf13/pflag v1.0.5
	go.uber.org/zap v1.17.0
	golang.org/x/crypto v0.0.0-20220411220226-7b82a4e95df4
	google.golang.org/protobuf v1.30.0
	gopkg.in/telebot.v3 v3.1.3
)

require (
	github.com/fatih/color v1.14.1 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/hokaccha/go-prettyjson v0.0.0-20211117102719-0474bc63780f // indirect
	github.com/iancoleman/strcase v0.2.0 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/lib/pq v1.10.9 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/text v0.8.0 // indirect
	google.golang.org/genproto v0.0.0-20230306155012-7f2fa6fef1f4 // indirect
	google.golang.org/grpc v1.55.0 // indirect
)

replace github.com/nedpals/postgrest-go => github.com/nedpals/postgrest-go v0.1.3
